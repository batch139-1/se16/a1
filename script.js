let count = prompt(`How many loops do you want?`)
console.log(`The number you provided is ${count}`)
console.log(parseInt(count))

for (count; count>=0; count--){
    if(count<=50){
    console.log(`The current value is at ${count}.\nTerminating the loop.`)
    break;
    } else if(count % 10 == 0){
        console.log(`The number is being skipped`)
        continue;
    } else if (count % 5 == 0) {
        console.log(count)
    }
}

// item 8 onwards

let string = `supercalifragilisticexpialidocious`
console.log(string)
let consonant = ``

for(i=0; i<string.length; i++){
    if (string[i] !== `a` && string[i] !== `e` && string[i] !== `i` && string[i] !== `o` && string[i] !== `u`){
        consonant += string[i]
    }
}
console.log(consonant)